package com.mastertech.autenticacao.controller;

import java.util.HashSet;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mastertech.autenticacao.model.Usuario;
import com.mastertech.autenticacao.repository.UsuarioRepository;
import com.mastertech.service.PasswordService;

@Controller
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@RequestMapping(path="/cadastrar", method=RequestMethod.POST)
	public @ResponseBody Usuario salvar(@RequestBody Usuario usuario) {
		String hash = passwordService.encode(usuario.getSenha());
		usuario.setSenha(hash);
		
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> logar(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBanco = usuarioRepository.findByEmail(usuario.getEmail());	
		
		if(! usuarioBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		boolean deuCerto = passwordService.verificar(usuario.getSenha(), usuarioBanco.get().getSenha());
		
		if(deuCerto) {
			return ResponseEntity.ok(usuarioBanco);
		}
		
		return ResponseEntity.badRequest().build();
	}
}
